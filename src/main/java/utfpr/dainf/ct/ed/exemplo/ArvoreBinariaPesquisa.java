package utfpr.dainf.ct.ed.exemplo;

/**
 * UTFPR - Universidade Tecnológica Federal do Paraná
 * DAINF - Departamento Acadêmico de Informática
 * 
 * Exemplo de implementação de árvore binária de pesquisa.
 * @author Wilson Horstmeyer Bogado <wilson@utfpr.edu.br>
 * @param <E> O tipo do valor armazenado nos nós da árvore
 */
public class ArvoreBinariaPesquisa<E extends Comparable<E>> extends ArvoreBinaria<E> {
    protected ArvoreBinariaPesquisa<E> pai;

    /**
     * Cria uma árvore com valor nulo na raiz.
     */
    public ArvoreBinariaPesquisa() {
    }

    /**
     * Cria uma árvore com o valor especificado na raiz.
     * @param valor O valor armazenado na raiz.
     */
    public ArvoreBinariaPesquisa(E valor) {
        super(valor);
    }

    /**
     * Inicializa o nó pai deste nó.
     * @param pai O nó pai.
     */
    protected void setPai(ArvoreBinariaPesquisa<E> pai) {
        this.pai = pai;
    }

    /**
     * Retorna o nó pai deste nó.
     * @return O nó pai.
     */
    protected ArvoreBinariaPesquisa<E> getPai() {
        return pai;
    }

    /**
     * Retorna o nó da árvore cujo valor corresponde ao especificado.
     * @param valor O valor a ser localizado.
     * @return A raiz da subárvore contendo o valor ou {@code null}.
     * 
     * ######
     * ##########
     * ############
     * ###############
     * #################
     * ##################
     * ###################
     * ####################
     * #####################
     * ######################
     * ##########s############
     * #########S s############
     * ########S   s###########
     * #######S     s###########
     * ######S       S##########
     * #######s     S############
     * ########s   S#############
     * #########s S##############
     * ##########s################
     * ###########################
     * ###########################
     * ###########################
     * ###########################
     * ########## @sau ###########
     */
     
    // O ORIGINAL ERA
    // public E pesquisa(E valor)/////////////////////////////// pode mudar?
    public ArvoreBinariaPesquisa<E> pesquisa(E valor){
        
        // Quando a raiz tiver o valor procurado
        if(valor == this.valor)
            return this;
        
        // Procura no lado esquerdo (se for menor)
        if(this.valor.compareTo(valor) < 0 && direita != null)
            return ((ArvoreBinariaPesquisa<E>)direita).pesquisa(valor);
        // ou no direito (se for maior)
        else if (esquerda != null)
            return ((ArvoreBinariaPesquisa<E>) esquerda).pesquisa(valor);
        
        // Não achou o valor
        return null;
    }
    
    /* ###########################
    *  #########################
    *  #####################
    *  ###############
    *  ##########
    *  ######
    *  ###
    *  ##
    *  #
    *  
    */

    /**
     * Retorna o nó da árvore com o menor valor.
     * @return A raiz da subárvore contendo o valor mínimo
     * 
     * ######
     * ##########
     * ############
     * ###############
     * #################
     * ##################
     * ###################
     * ####################
     * #####################
     * ######################
     * ##########s############
     * #########S s############
     * ########S   s###########
     * #######S     s###########
     * ######S       S##########
     * #######s     S############
     * ########s   S#############
     * #########s S##############
     * ##########s################
     * ###########################
     * ###########################
     * ###########################
     * ###########################
     * ########## @sau ###########
     */
     
    public ArvoreBinariaPesquisa<E> getMinimo() {
        
        // Iterador para encontrar o menor valor
        ArvoreBinariaPesquisa<E> menor;
        menor = this;
        
        // O iterador procura o nó mais à esquerda possível
        while(menor != null && menor.esquerda != null)
            menor = (ArvoreBinariaPesquisa) menor.esquerda;
        
        return menor;
    }
    
    /* ###########################
    *  #########################
    *  #####################
    *  ###############
    *  ##########
    *  ######
    *  ###
    *  ##
    *  #
    *  
    */

    /**
     * Retorna o nó da árvore com o maior valor.
     * @return A raiz da subárvore contendo o valor máximo
     * 
     * ######
     * ##########
     * ############
     * ###############
     * #################
     * ##################
     * ###################
     * ####################
     * #####################
     * ######################
     * ##########s############
     * #########S s############
     * ########S   s###########
     * #######S     s###########
     * ######S       S##########
     * #######s     S############
     * ########s   S#############
     * #########s S##############
     * ##########s################
     * ###########################
     * ###########################
     * ###########################
     * ###########################
     * ########## @sau ###########
     */
     
    public ArvoreBinariaPesquisa<E> getMaximo(){
        
        // Iterador para encontrar o menor valor
        ArvoreBinariaPesquisa<E> maior;
        maior = this;
        
        // O iterador procura o nó mais à direita possível
        while(maior != null && maior.direita != null)
            maior = (ArvoreBinariaPesquisa) maior.direita;
        
        return maior;
    }
    
    /* ###########################
    *  #########################
    *  #####################
    *  ###############
    *  ##########
    *  ######
    *  ###
    *  ##
    *  #
    *  
    */

    /**
     * Retorna o nó sucessor do nó especificado.
     * @param no O nó cujo sucessor desejamos localizar
     * @return O sucessor do no ou {@null}.
     * 
     * ######
     * ##########
     * ############
     * ###############
     * #################
     * ##################
     * ###################
     * ####################
     * #####################
     * ######################
     * ##########s############
     * #########S s############
     * ########S   s###########
     * #######S     s###########
     * ######S       S##########
     * #######s     S############
     * ########s   S#############
     * #########s S##############
     * ##########s################
     * ###########################
     * ###########################
     * ###########################
     * ###########################
     * ########## @sau ###########
     */
     
    public ArvoreBinariaPesquisa<E> sucessor(ArvoreBinariaPesquisa<E> no) {
        
        // Se houver uma sub-àrvore direita, ele retorna o mínimo a partir dela.
        if(no != null && no.direita != null)
            return ((ArvoreBinariaPesquisa)no.direita).getMinimo();
        
        // Iteradores
        ArvoreBinariaPesquisa<E> filho = no;
        ArvoreBinariaPesquisa<E> pai;
        if(no != null)
            pai = no.pai;
        else
            pai = null;
  
        // Subindo a árvore enquanto for filho direito de alguém.
        while(pai != null && filho == pai.direita){
            filho = pai;
            pai = pai.pai;
        }
            
        return pai;
    }
    
    /* ###########################
    *  #########################
    *  #####################
    *  ###############
    *  ##########
    *  ######
    *  ###
    *  ##
    *  #
    *  
    */

    /**
     * Retorna o nó predecessor do nó especificado.
     * @param no O nó cujo predecessor desejamos localizar
     * @return O predecessor do nó ou {@null}.
     * 
     * ######
     * ##########
     * ############
     * ###############
     * #################
     * ##################
     * ###################
     * ####################
     * #####################
     * ######################
     * ##########s############
     * #########S s############
     * ########S   s###########
     * #######S     s###########
     * ######S       S##########
     * #######s     S############
     * ########s   S#############
     * #########s S##############
     * ##########s################
     * ###########################
     * ###########################
     * ###########################
     * ###########################
     * ########## @sau ###########
     */
     
    public ArvoreBinariaPesquisa<E> predecessor(ArvoreBinariaPesquisa<E> no) {
        
        // Se houver uma sub-àrvore esquerda, ele retorna o mínimo a partir dela.
        if(no != null && no.esquerda != null)
            return ((ArvoreBinariaPesquisa<E>)no.esquerda).getMinimo();
        
        // Iteradores
        ArvoreBinariaPesquisa<E> filho = no;
        ArvoreBinariaPesquisa<E> pai;
        if(no != null)
            pai = no.pai;
        else
            pai = null;
  
        // Subindo a árvore enquanto for filho esquerdo de alguém.
        while(pai != null && filho == pai.esquerda){
            filho = pai;
            pai = pai.pai;
        }
            
        return pai;
    }
    
    /* ###########################
    *  #########################
    *  #####################
    *  ###############
    *  ##########
    *  ######
    *  ###
    *  ##
    *  #
    *  
    */

    /**
     * Insere um nó contendo o valor especificado na árvore.
     * @param valor O valor armazenado no nó.
     * @return O nó inserido
     * 
     * ######
     * ##########
     * ############
     * ###############
     * #################
     * ##################
     * ###################
     * ####################
     * #####################
     * ######################
     * ##########s############
     * #########S s############
     * ########S   s###########
     * #######S     s###########
     * ######S       S##########
     * #######s     S############
     * ########s   S#############
     * #########s S##############
     * ##########s################
     * ###########################
     * ###########################
     * ###########################
     * ###########################
     * ########## @sau ###########
     */
     
    public ArvoreBinariaPesquisa<E> insere(E valor) {
        
        if(this.pesquisa(valor) == null){
            // Alocação do nó a ser inserido
            ArvoreBinariaPesquisa<E> no = new ArvoreBinariaPesquisa<>();
            no.valor = valor;
            no.pai = null;
            no.esquerda = null;
            no.direita = null;

            // Raiz a ser retornada
            ArvoreBinariaPesquisa<E> raiz = new ArvoreBinariaPesquisa<>();
            raiz = this;

            // Procura o pai do nó, se ele tiver um. :c
            while(raiz != null){
                no.pai = raiz;
                if(no.valor.compareTo(raiz.valor) < 0)
                    raiz = (ArvoreBinariaPesquisa<E>) raiz.esquerda;
                else
                    raiz = (ArvoreBinariaPesquisa<E>) raiz.direita;
            }

            // O pai não existirá quando a arvore for vazia.
            if(no.pai == null)
                raiz = no;
            else{
                // Referencia o nó no pai.
                if(no.valor.compareTo(no.pai.valor) < 0)
                    no.pai.esquerda = no;
                else
                    no.pai.direita = no;
            }

            return no;
            
        }
        
        return null;
        
    }
    
    /* ###########################
    *  #########################
    *  #####################
    *  ###############
    *  ##########
    *  ######
    *  ###
    *  ##
    *  #
    *  
    */

    /**
     * Exclui o nó especificado da árvore.
     * Se a raiz for excluída, retorna a nova raiz.
     * @param no O nó a ser excluído.
     * @return A raiz da árvore
     * 
     * ######
     * ##########
     * ############
     * ###############
     * #################
     * ##################
     * ###################
     * ####################
     * #####################
     * ######################
     * ##########s############
     * #########S s############
     * ########S   s###########
     * #######S     s###########
     * ######S       S##########
     * #######s     S############
     * ########s   S#############
     * #########s S##############
     * ##########s################
     * ###########################
     * ###########################
     * ###########################
     * ###########################
     * ########## @sau ###########
     */
     
    public ArvoreBinariaPesquisa<E> exclui(ArvoreBinariaPesquisa<E> no) {
    
        // já recebemos o nó
        no = pesquisa(no.valor);
        ArvoreBinariaPesquisa<E> resultado = no;
       
       // se o nó não possui filhos
       if( resultado.direita == null && resultado.esquerda == null )
       {
           if( resultado.pai == null ) // é a raiz alone
           {
               no.valor = null;
           }
           else
           {
               if( resultado == resultado.pai.direita )
                    resultado.pai.direita = null;
                else if( resultado == resultado.pai.esquerda )
                    resultado.pai.esquerda = null;
           }
       }
       
        // o nó possui apenas um filho
        else if( resultado.direita != null && resultado.esquerda == null ) // sabemos que não será a raiz vazia
        {
            if( resultado.pai != null ) // sabemos que não é a raiz
            {
                ((ArvoreBinariaPesquisa<E>)resultado.direita).pai = resultado.pai; // arrumando o pai do filho
                if( resultado == resultado.pai.direita )
                    resultado.pai.direita = resultado.direita;
                else if( resultado == resultado.pai.esquerda )
                    resultado.pai.esquerda = resultado.direita;
            }
            else // se o nó não tem pai estamos deletando a raiz
            {
                resultado.valor = resultado.direita.valor;
                resultado.direita = null;
            }
        }
       else if( resultado.direita == null && resultado.esquerda != null ) // sabemos que não será a raiz vazia
        {
            if( resultado.pai != null )
            {
                ((ArvoreBinariaPesquisa<E>)resultado.esquerda).pai = resultado.pai; // arrumando o pai do filho
                if( resultado == resultado.pai.direita )
                    resultado.pai.direita = resultado.esquerda;
                else if( resultado == resultado.pai.esquerda )
                    resultado.pai.esquerda = resultado.esquerda;
            }
            else // se o nó não tem pai estamos deletando a raiz
            {
                resultado.valor = resultado.esquerda.valor;
                resultado.esquerda = null;
            }
        }
       
       // o nó possui dois filhos
       else if( resultado.direita != null && resultado.esquerda != null )
       {
           while( resultado.direita != null && resultado.esquerda != null ) // tem no máximo um filho
           {
               resultado = sucessor(resultado); // pegando o sucessor do nó
           }
           exclui(resultado);
           no.valor = resultado.valor;

       }
       return this; // retorna sempre a raiz da árvore
        
    }
    
    /* ###########################
    *  #########################
    *  #####################
    *  ###############
    *  ##########
    *  ######
    *  ###
    *  ##
    *  #
    *  
    */
}
